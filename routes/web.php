<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * For production we can improve routes system using route groups etc.
 */

Route::get(
    '/',
    'ContactsController@dashboard'
);

Route::get(
    '/contact/fetch',
    'ContactsController@fetch'
);

Route::get(
    '/contact/get/{id}',
    'ContactsController@get'
)->where('id', '[0-9]+');

Route::post(
    '/contact/add/',
    'ContactsController@add'
);

Route::post(
    '/contact/save/{id}',
    'ContactsController@save'
)->where('id', '[0-9]+');

Route::get(
    '/contact/delete/{id}',
    'ContactsController@delete'
)->where('id', '[0-9]+');
