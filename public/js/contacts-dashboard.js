
/**
 * Decided not to spend a lot of time on frontend.
 * For production we can use some frontend framework or other
 * more extandable solution than provided at the moment.
 */

let contactFormPopup;
let contactFormPopupBtnClose;
let contactFormPopupBtnCreate;
let contactFormPopupBtnUpdate;

let contactFormCurrentId;

$(document).ready(function(){

  $("[data-toggle=tooltip]").tooltip();

    contactFormTemplate = $("#contact-form-template").html();

    contactFormPopupBtnClose = {
      label: 'Close',
      cssClass: 'btn btn-default',
      action: function(dialog) {
          dialog.close();
          window.location.href=window.location.href;
          contactFormCurrentId = null;
      }
    };

    contactFormPopupBtnCreate = {
      label: 'Create',
      cssClass: 'btn btn-primary',
      autospin: true,
      action: function(dialog) {
        var email = dialog.getModalBody().find('#contact-email').val();
        var firstName = dialog.getModalBody().find('#contact-firstName').val();
        var lastName = dialog.getModalBody().find('#contact-lastName').val();
        var phone = dialog.getModalBody().find('#contact-phone').val();
        contactCreate(email, firstName, lastName, phone);
      }
    };

    contactFormPopupBtnUpdate = {
      label: 'Update',
      cssClass: 'btn btn-primary',
      autospin: true,
      action: function(dialog) {
        var email = dialog.getModalBody().find('#contact-email').val();
        var firstName = dialog.getModalBody().find('#contact-firstName').val();
        var lastName = dialog.getModalBody().find('#contact-lastName').val();
        var phone = dialog.getModalBody().find('#contact-phone').val();
        contactSave(contactFormCurrentId, email, firstName, lastName, phone);
      }
    };

    contactFormPopup = new BootstrapDialog({
      label: 'Create',
      cssClass: 'modal modal-primary',
      autospin: true,
      action: function(dialog) {
        var email = dialog.getModalBody().find('#contact-email').val();
        var firstName = dialog.getModalBody().find('#contact-firstName').val();
        var lastName = dialog.getModalBody().find('#contact-lastName').val();
        var phone = dialog.getModalBody().find('#contact-phone').val();
        contactCreate(email, firstName, lastName, phone);
      }
    });


  $(".contact-btn-create").click(function () {
    contactFormPopup.close();
    contactFormPopup.setTitle('Create New Contact');
    contactFormPopup.setMessage(contactFormTemplate);
    contactFormPopup.setButtons([
      contactFormPopupBtnCreate,
      contactFormPopupBtnClose
    ]);
    contactFormPopup.open();
    contactFormPopup.updateButtons();
    contactFormPopup.setClosable(false);
  });

  $(".contact-btn-edit").click(function () {
    contactFormCurrentId = $(this).attr('data-id');

    contactFormPopup.close();

    contactFormDataGet(contactFormCurrentId);

    contactFormPopup.setTitle('Update Contact');

    contactFormPopup.setButtons([]);

    contactFormPopup.setMessage('Please Wait. Loading...');

    contactFormPopup.open();
    contactFormPopup.updateButtons();
    contactFormPopup.setClosable(false);
  });

  $(".contact-btn-delete").click(function () {
    var id = $(this).attr('data-id');
    BootstrapDialog.confirm('Are you sure?', function(result){
        if(result) {
            return contactDelete(id);
        }else {
            return false;
        }
    });
  });

}); // ready end

/**
 * Functions:
 */

function contactCreate(email, firstName, lastName, phone) {
  contactFormPopup.setMessage('Please Wait. Loading...');
  $.post(
    '/contact/add',
    {
      email: email,
      firstName: firstName,
      lastName: lastName,
      phone: phone
    }
  ).done(function(data) {
    if (data.status == 'Ok') {
      contactFormPopup.setMessage( '<div class="alert alert-success text-center">'+data.message+'</div>' );
    } else {
      contactFormPopup.setMessage( '<div class="alert alert-danger text-center">'+data.message+'</div>' );
    }
    contactFormPopup.setClosable(true);
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  })
  .fail(function() {
    contactFormPopup.setClosable(true);
    contactFormPopup.setMessage( '<div class="alert alert-danger text-center">Error</div>' );
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  });
}

function contactFormDataGet(id) {
  $.ajax({
    url: '/contact/get/'+id,
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    if (data.status == 'Ok') {
      contactFormPopup.setMessage(contactFormTemplate);
      contactFormPopup.getModalBody().find('#contact-email').val(data.data.email);
      contactFormPopup.getModalBody().find('#contact-firstName').val(data.data.firstName);
      contactFormPopup.getModalBody().find('#contact-lastName').val(data.data.lastName);
      contactFormPopup.getModalBody().find('#contact-phone').val(data.data.phoneNumber);
      contactFormPopup.setButtons([
        contactFormPopupBtnUpdate,
        contactFormPopupBtnClose
      ]);
      contactFormPopup.updateButtons();
    } else {
      contactFormPopup.setClosable(true);
      contactFormPopup.setMessage( '<div class="alert alert-danger text-center">'+data.message+'</div>' );
    }
  })
  .fail(function() {
    contactFormPopup.setClosable(true);
    contactFormPopup.setMessage( '<div class="alert alert-danger text-center">Error</div>' );
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  });

}


function contactSave(id, email, firstName, lastName, phone) {
  contactFormPopup.setMessage('Please Wait. Loading...');
  $.post(
    '/contact/save/'+id,
    {
      email: email,
      firstName: firstName,
      lastName: lastName,
      phone: phone
    }
  ).done(function(data) {
    if (data.status == 'Ok') {
      contactFormPopup.setMessage( '<div class="alert alert-success text-center">'+data.message+'</div>' );
    } else {
      contactFormPopup.setMessage( '<div class="alert alert-danger text-center">'+data.message+'</div>' );
    }
    contactFormPopup.setClosable(true);
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  })
  .fail(function() {
    contactFormPopup.setClosable(true);
    contactFormPopup.setMessage( '<div class="alert alert-danger text-center">Error</div>' );
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  });
}

function contactDelete(id) {
  contactFormPopup.setTitle('Delete Contact');
  contactFormPopup.setMessage('Please wait. Loading...');
  contactFormPopup.setButtons([]);
  contactFormPopup.open();
  contactFormPopup.updateButtons();
  contactFormPopup.setClosable(false);
  $.ajax({
    url: '/contact/delete/'+id,
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    if (data.status == 'Ok') {
      contactFormPopup.setClosable(true);
      contactFormPopup.setMessage( '<div class="alert alert-success text-center">'+data.message+'</div>' );
      contactFormPopup.setButtons([
        contactFormPopupBtnClose
      ]);
      contactFormPopup.updateButtons();
    } else {
      contactFormPopup.setClosable(true);
      contactFormPopup.setMessage( '<div class="alert alert-danger text-center">'+data.message+'</div>' );
    }
  })
  .fail(function() {
    contactFormPopup.setClosable(true);
    contactFormPopup.setMessage( '<div class="alert alert-danger text-center">Error</div>' );
    contactFormPopup.setButtons([
      contactFormPopupBtnClose
    ]);
  });
}
