<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Here is Eloquent Model for `contacts` table.
 * It can be improved by using Laravel Observer.
 * I think it would be more pretty and clean with it,
 * because ZohoContacts methods would be implemented
 * in the Observer.
 * Also for production we will need some sort of data
 * validation and more exact errors/warnings outputs.
 */
class Contacts extends Model
{
    protected $table = 'contacts';
    protected $fillable = [
        'email',
        'last_name',
        'first_name',
        'phone'
    ];

    public static function createContact(
        string $email,
        string $firstName,
        string $lastName,
        string $phoneNumber,
        int $zohoId = null
    ) {
        $contact = new Contacts();

        if (empty($zohoId)) {
            $contact->zoho_id = ZohoContacts::model()->create(
                $email,
                $firstName,
                $lastName,
                $phoneNumber
            );
        } else {
            $contact->zoho_id = $zohoId;
        }

        if (empty($contact->zoho_id)) {
            return null;
        }

        $contact->email = $email;
        $contact->last_name = $lastName;
        $contact->first_name = $firstName;
        $contact->phone = $phoneNumber;

        $contact->save();

        return $contact;
    }

    public static function readContact(int $id)
    {
        if (empty($id)) {
            return null;
        }

        return Contacts::find($id);
    }

    public static function readContacts()
    {
        return Contacts::all();
    }

    public static function updateContact(
        int $id,
        string $email,
        string $firstName,
        string $lastName,
        string $phoneNumber
    ) {
        if (empty($id)) {
            return null;
        }

        $contact = Contacts::readContact($id);
        if (empty($contact->id)) {
            return null;
        }

        $zoho_updated = ZohoContacts::model()->update(
            $contact->zoho_id,
            $email,
            $lastName,
            $firstName,
            $phoneNumber
        );

        if (!$zoho_updated) {
            return null;
        }

        $contact->update([
            'email' => $email,
            'last_name' => $lastName,
            'first_name' => $firstName,
            'phone' => $phoneNumber
        ]);

        return $contact;
    }

    public static function deleteContact(int $id)
    {
        if (empty($id)) {
            return null;
        }

        $contact = Contacts::readContact($id);
        if (empty($contact->id)) {
            return null;
        }

        $zoho_deleted = ZohoContacts::model()->delete(
            $contact->zoho_id
        );

        if (!$zoho_deleted) {
            return null;
        }

        $contact->delete();

        return $contact;
    }

    public static function getContactByZohoId(int $zohoId)
    {
        $contacts_result = Contacts::where('zoho_id', $zohoId);
        if (!empty($contacts_result)) {
            return $contacts_result->first();
        }
    }

    public static function fetchZohoContacts()
    {
        $zohoRecords = ZohoContacts::model()->getAll();
        if (empty($zohoRecords)) {
            return null;
        }

        $fetchedCnt = count($zohoRecords);

        foreach ($zohoRecords as $zohoRecord) {
            // check if we have it in internal DB:
            $contact = Contacts::getContactByZohoId($zohoRecord['CONTACTID']);

            if (!empty($contact)) {
                // if exists - just update fields
                Contacts::updateContact(
                    $contact->id,
                    $zohoRecord['Email'],
                    $zohoRecord['First Name'],
                    $zohoRecord['Last Name'],
                    $zohoRecord['Phone']
                );
            } else {
                // if no - create
                Contacts::createContact(
                    $zohoRecord['Email'],
                    $zohoRecord['First Name'],
                    $zohoRecord['Last Name'],
                    $zohoRecord['Phone'],
                    $zohoRecord['CONTACTID']
                );
            }
        }
        unset($zohoRecord);

        return $fetchedCnt;
    }
}
