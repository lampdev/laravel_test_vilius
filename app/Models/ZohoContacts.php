<?php

namespace App\Models;

use Maidmaid\Zoho\Client;

/**
 * Here is the class to work with Zoho library.
 * It provides CRUD possibilities and system uses it
 * when works with Contacts model.
 * I think that it can be improved to work with
 * different Zoho CRM Modules, not only with Contacts.
 * But need more time to investigate about their API
 * and used Maidmaid\Zoho\Client library.
 */
class ZohoContacts
{
    const MODULE = 'Contacts';

    private $zoho;

    private static $_instance;

    public function __construct()
    {
        $this->zoho = new Client(env('ZOHO_AUTHTOKEN'));
    }

    public static function model()
    {
        // singleton implementation
        if (self::$_instance === null) {
            self::$_instance = new ZohoContacts();
        }
        return self::$_instance;
    }


    public function create(
        string $email,
        string $firstName,
        string $lastName,
        string $phoneNumber
    ) {
        $id = null;

        $records = $this->zoho->insertRecords(
            self::MODULE,
            $data = [
                10 => [
                    'Email' => $email,
                    'Last Name' => $lastName,
                    'First Name' => $firstName,
                    'Phone' => $phoneNumber,
                ]
            ]
        );

        if (!empty($records[10]['Id'])) {
            $id = $records[10]['Id'];
        }

        return $id;
    }


    public function getById(int $id = null)
    {
        $contact = null;
        if (empty($id)) {
            return $contact;
        }

        $records = $this->zoho->getRecordById(
            self::MODULE,
            [
                $id
            ]
        );

        if (!empty($records[1])) {
            $contact = $records[1];
        }

        return $contact;
    }

    public function getAll()
    {
        $contacts = $this->zoho->getRecords(
            self::MODULE
        );
        return $contacts;
    }


    public function update(
        int $id,
        string $email,
        string $firstName,
        string $lastName,
        string $phoneNumber
    ) {
        $records = $this->zoho->updateRecords(
            self::MODULE,
            $data = [
                10 => [
                    'Id' => $id,
                    'First Name' => $firstName,
                    'Last Name' => $lastName,
                    'Phone' => $phoneNumber,
                    'Email' => $email,
                ]
            ]
        );

        return (!empty($records[10]['Id']));
    }


    public function delete(int $id)
    {

        /**
         *
         * There is an issue with external lib (ZohoClient)
         * It throws PHP Notices.
         * It will be helpful to fix before production =)
         *
         */

        $delete = @$this->zoho->deleteRecords(
            self::MODULE,
            $id
        );

        return true;
    }
}
