<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maidmaid\Zoho\Client;
use Illuminate\Support\Facades\Input;

use App\Models\Contacts;

/**
 * Here is the Controller Class. Before production
 * I would like to move ajax actions to the new controller
 * and create there more clean request/response handling.
 */
class ContactsController extends Controller
{

    /**
     * Here is the main page.
     */
    public function dashboard()
    {

        $contacts = Contacts::readContacts();

        return view('ContactsDashboard', ['contacts' => $contacts]);
    }

    public function get($id)
    {
        $contact = Contacts::readContact($id);

        if (empty($contact->id)) {
            return response()->json([
                'status' => 'Error',
                'message' => 'User is not found.',
                'data' => []
            ]);
        } else {
            return response()->json([
                'status' => 'Ok',
                'message' => 'Success',
                'data' => [
                    'email' => $contact->email,
                    'firstName'     => $contact->first_name,
                    'lastName'      => $contact->last_name,
                    'phoneNumber'   => $contact->phone
                ]
            ]);
        }
    }

    public function add(Request $request)
    {
        /**
         * For the production should be used laravel forms with validation.
         */
        $data = Input::only('email', 'lastName', 'firstName', 'phone');
        $contact = Contacts::createContact(
            $data['email'],
            $data['firstName'],
            $data['lastName'],
            $data['phone']
        );

        if (empty($contact->id)) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Error Creating Contact.'
            ]);
        } else {
            return response()->json([
                'status' => 'Ok',
                'message' => 'Success.'
            ]);
        }
    }

    public function save(Request $request, $id)
    {
        $data = Input::only('email', 'lastName', 'firstName', 'phone');
        $contact = Contacts::updateContact(
            $id,
            $data['email'],
            $data['firstName'],
            $data['lastName'],
            $data['phone']
        );

        if (empty($contact->id)) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Error Saving Contact.'
            ]);
        } else {
            return response()->json([
                'status' => 'Ok',
                'message' => 'Success.'
            ]);
        }
    }

    public function delete(Request $request, $id)
    {
        $contact = Contacts::deleteContact($id);

        if (empty($contact->id)) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Error Deleting Contact.'
            ]);
        } else {
            return response()->json([
                'status' => 'Ok',
                'message' => 'Success.'
            ]);
        }
    }

    /**
     * Fetch function would be also used via ajax.
     * $fetchedCnt returns the number of fetched records
     * from Zoho.
     */
    public function fetch(Request $request)
    {
        $fetchedCnt = Contacts::fetchZohoContacts();

        return redirect('/');
    }
}
