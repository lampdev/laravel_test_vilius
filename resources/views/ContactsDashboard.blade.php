<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contacts Dashboard</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <script src="/js/contacts-dashboard.js?v=123456789"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    <h4 class="">Contacts</h4>
                </div>

                <div class = "panel-body">


{{-- It would be better for production to setup layouts, partials and other viewer's components. --}}





<a href="/contact/fetch" class="btn btn-success btn-md pull-left" data-title="Fetch CRM Contacts">
    <span class="glyphicon glyphicon-refresh"></span>
    Fetch CRM Contacts
</a>
<button class="btn btn-primary btn-md pull-right contact-btn-create" data-title="Create New Contact">
    <span class="glyphicon glyphicon-plus"></span>
    Create New Contact
</button>
<div class="clearfix"></div>
<br>

@if (empty($contacts) || (count($contacts) == 0))
    <div class="alert alert-warning">There are no contacts in the DB. Please add the new one or fetch from CRM.</div>
@else
    <div class="table-responsive">

        <table id="contacts-table" class="table table-bordred table-striped">

            <thead>
                <!-- <th><input type="checkbox" id="contacts-checkall" /></th> -->
                <th>Email</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>

            <tbody>
                @foreach ($contacts as $contact)
                    <tr>
                        <!-- <td><input type="checkbox" class="contacts-checkthis" /></td> -->
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->first_name}}</td>
                        <td>{{$contact->last_name}}</td>
                        <td>{{$contact->phone}}</td>
                        <td>
                            <p data-placement="top" data-toggle="tooltip" title="Edit">
                                <button class="btn btn-primary btn-xs contact-btn-edit" data-id="{{$contact->id}}">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </button>
                            </p>
                        </td>
                        <td>
                            <p data-placement="top" data-toggle="tooltip" title="Delete">
                                <button class="btn btn-danger btn-xs contact-btn-delete" data-id="{{$contact->id}}" >
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </p>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>

        <!-- <div class="clearfix"></div>
        <ul class="pagination pull-right">
          <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
        </ul> -->

    </div>
@endif




                </div>
            </div>
        </div>
    </div>
</div>

{{--
Didn't want to spend a lot of time and created w/o Laravel forms.
For production it will be better to use them.
 --}}

<div id="contact-form-template" style="display:none;">
    Email: <input id="contact-email" type="text" class="form-control" \>
    First Name: <input id="contact-firstName" type="text" class="form-control" \>
    Last Name: <input id="contact-lastName" type="text" class="form-control" \>
    Phone Number: <input id="contact-phone" type="text" class="form-control" \>
</div>

</body>
</html>
