# Laravel Test Vilius

The project provides a possibility to create/read/update/delete Contacts records from Zoho CRM.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Environment Requirements

The next requirements should be satisfied by your system:
Composer Requirements [Link](https://getcomposer.org/doc/00-intro.md#system-requirements)
Laravel 5.4 Requirements [Link](https://laravel.com/docs/5.4#server-requirements)

### Installing

Setup your web server host directory to `{project_dir}/public`

Composer install command from `{project_dir}` directory

```
composer install
```

Make your `.env` file by example from `.env.example` file

Generate Laravel App Key
```
php artisan key:generate
```
Create your AuthToken at Zoho [Link](https://www.zoho.eu/crm/help/api/using-authentication-token.html#Generate_Auth_Token)

Add your AuthToken to `.env` file
to field `ZOHO_AUTHTOKEN`
Make Laravel migration

```
 php artisan migrate
```

Done.
You can check the project via browser.

## Running the tests

To run the automated tests for this project you need to install phpunit [Link](https://phpunit.de/manual/current/en/installation.html)

run command to test from your project directory:
```
phpunit
```

### At the end of tests 

at file `test/Feature/ContactsTest.php` located our test

```
-   test pass data from controller to main view;
-   test post request for adding new user;
-   test post request for updating user information;
-   test request for deleting user;
-   test json response for deleting user;
-   test 200 status of some main Route;
```

## Built With

* [Laravel](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maidmaid/Zoho](https://github.com/maidmaid/zoho) - Zoho client library

## Authors

* **LampDev** - [our web-site](https://lamp-dev.com/)
