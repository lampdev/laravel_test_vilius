<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testViewHaveData()
    {
        $response = $this->get('/');

        $response->assertViewHas('contacts');
    }

    public function testPostToAdd()
    {
        $response = $this->post('/contact/add/',[
            'email' => 'asdasdasd@asdasda.com',
            'lastName' => 'Fsasdae',
            'firstName' => 'lasdsls',
            'phone' => '2313122432'
        ]);

        $response->assertStatus(200);
    }

    public function testUserGet()
    {
        $response = $this->get('/contact/get/1');

        $response->assertStatus(200);
    }

    public function testPostToSave()
    {
        $response = $this->post('/contact/save/1',[
            'email' => 'dfdf@sd.com',
            'lastName' => 'Frere',
            'firstName' => 'lksls',
            'phone' => '2342432'
        ]);

        $response->assertStatus(200);
    }

    public function testDelete()
    {
        $response = $this->get('/contact/delete/1');

        $response->assertStatus(200);
    }

    public function testJsonOnDelete()
    {
        $response = $this->get('/contact/delete/1');

        $response->assertJson([
                'status' => 'Error',
                'message' => 'Error Deleting Contact.'
            ]);
    }
}
